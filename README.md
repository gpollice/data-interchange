# Data Interchange in Java Tutorial
This Eclipse project contains a basic tutorial of how to use some standard data interchange formats to serialize Java objects to a textual representation and vice versa. This project was designed for use in my CS4233 Object-oriented Analysis & Design course at Worcester Polytechnic Institute.

## Overview
Data serialization and deserialization between Java objects and a textual representation is a common technique in many applications. Text is easy to store and transfer between applications. Each application can convert the text to objects and store objects as text using standard libraries. This allows objects to persist between executions or be converted for use in databases or other software.

Several *standard* formats for representing structured data have been defined and libraries that provide interchange between text and other forms, such as Java objects, developed. We look at three test formats: XML, JSON, and YAML. We show how to serialize text to object and deserialize the objects back to text using some popular libraries like [Google GSON][1] and Jackson.

The example(s) used for this tutorial are variations on a simple program that delivers menus to the user. The basic scenario is that the user will ask for a menu for the day and receive the menu that contains instructions for how to make the main course, appetizer, etc. We will not have a complete application, but work our way through some of the variations that we might encounter and how to use techniques, including dependency injection to make our application more flexible and configurable.

### Code structure
We go through several versions of the menu maker application. All of these are rooted in the **ditut** package. This root package contains sub-packages for each version, starting with **menu01** and ending with **menu10**.  Everything under these packages forms the complete code base for a section in the tutorial.

There are two source folders for the project, **src** and **test**. For all versions there is a corresponding version package under **test** that corresponds to the code in the same named package in **src**. The unit tests for each version are in a test file for that version in the **test** folder. The tests are not extensive, but designed to show the dependency injection works.

All of the libraries that you need for this tutorial are in the **lib** folder. Google keeps changing some of its library contents and you need to make sure that the libraries are complete. If you use *maven*, *gradle*, or other dependency management and build automation tools, you should get a consistent set of libraries. I downloaded the libraries manually using the [Maven Central Repository](https://mvnrepository.com/repos/central) and the [Sonatype Maven Search facility](https://search.maven.org/) to determine what the appropriate dependent libraries were. However, these failed to include the *failureaccess-1.0.1.jar* which is necessary to create the Injector instances, and I have included it in the build path.

---

## References

\[1] Google GSON project on GitHub, https://github.com/google/gson.

[1]: https://github.com/google/gson "GSON on GitHub"