/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package interchange.common;

/**
 * Description
 * @version Jul 31, 2020
 */
public enum Piece
{
	// White pieces
	WP("P", "Pawn", "White", 1),
	WR("R", "Rook", "White", 5),
	WN("N", "Knight", "White", 3),
	WB("B", "Bishop", "White", 3),
	WQ("Q", "Queen", "White", 9),
	WK("K", "King", "White", Integer.MAX_VALUE),
	BP("p", "Pawn", "Black", 1),
	BR("r", "Rook", "Black", 5),
	BN("n", "Knight", "Black", 3),
	BB("b", "Bishop", "Black", 3),
	BQ("q", "Queen", "Black", 9),
	BK("k", "King", "Black", Integer.MAX_VALUE);
	
	private String abbreviation;
	private String pieceName;
	private String color;
	private int value;
	
	private Piece(String abbreviation, String pieceName, String color, int value)
	{
		this.abbreviation = abbreviation;
		this.pieceName = pieceName;
		this.color = color;
		this.value = value;
	}

	/**
	 * @return the abbreviation
	 */
	public String getAbbreviation()
	{
		return abbreviation;
	}

	/**
	 * @return the pieceName
	 */
	public String getPieceName()
	{
		return pieceName;
	}

	/**
	 * @return the color
	 */
	public String getColor()
	{
		return color;
	}

	/**
	 * @return the value
	 */
	public int getValue()
	{
		return value;
	}
	
	/**
	 * @return the full name (color piece)
	 */
	public String getFullName()
	{
		return color + " " + pieceName;
	}
}
