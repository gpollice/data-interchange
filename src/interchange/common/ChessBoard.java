/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package interchange.common;

import java.util.*;

/**
 * Description
 * @version Jul 31, 2020
 */
public class ChessBoard
{
	private BoardConfiguration theBoard;
	
	public ChessBoard(BoardConfiguration configuration)
	{
		theBoard = configuration;
	}
	
	public Optional<Piece> getPieceAt(Square s)
	{
		return theBoard.getPieceAt(s);
	}

	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return theBoard.toString();
	}
}
