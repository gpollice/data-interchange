/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package interchange.common;

/**
 * Skeleton Chess analyzer. Given a board, it analyzes the position.
 * @version Jun 26, 2020
 */
public class ChessAnalyzer
{
	
	/**
	 * Description
	 */
	public ChessAnalyzer()
	{
		// Initialize the analyzer
	}

	public void analyze(ChessBoard board)
	{
		// Do anything
		System.out.println(board.toString());
	}
}
