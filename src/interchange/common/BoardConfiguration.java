/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package interchange.common;

import java.util.*;

/**
 * A chess board configuration. This is just a data object.
 * @version Aug 5, 2020
 */
public class BoardConfiguration
{
	private Map<Square, Piece> configuration;
	
	public BoardConfiguration()
	{
		configuration = new HashMap<Square,Piece>();
	}
	
	public Optional<Piece> getPieceAt(Square s)
	{
		return Optional.ofNullable(configuration.get(s));
	}
	
	private Piece getPieceAt(int row, int col)
	{
		return configuration.get(new Square(row, col));
	}

	
	public void putPieceAt(Square s, Piece p)
	{
		configuration.put(s, p);
	}
	
	public void putPieceAt(int row, int col, Piece p)
	{
		configuration.put(new Square(row, col), p);
	}
	/*
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		final StringBuilder sb = new StringBuilder();
		final String hBorder = "+-----------------------------------------------+\n";
		sb.append(hBorder);
		for (int row = 8; row > 0; row--) {
			sb.append('|');
			for (int column = 1; column <= 8; column++) {
				sb.append(' ');
				Piece p = getPieceAt(row, column);
				sb.append(
					p == null ? "XX  |"
						: " " + p + " |"
					);
			}
			sb.append('\n');
			sb.append(hBorder);
		}
		return sb.toString();
	}
}
