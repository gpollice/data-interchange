/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package interchange;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import interchange.common.*;
import static interchange.common.Piece.*;

/**
 * Just shows that a Chessboard and a configuration actually work.
 * @version Jun 26, 2020
 */
class ChessBoardTest
{

	@Test
	void test()
	{
		BoardConfiguration bc = new BoardConfiguration();
		bc.putPieceAt(1, 5, WK);
		bc.putPieceAt(8, 5, BK);
		ChessBoard board = new ChessBoard(bc);
		String s = board.toString();
		System.out.println(s);
		assertNotNull(s);
	}

}
